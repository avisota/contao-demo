Avisota Demo Website
====================

Installation guide
------------------

* Install Contao 3.4.x
* Activate `Rewrite URLs` in the system settings.
* Activate `Enable folder URLs` in the system settings.
* Install `composer` extension via ER2 client
* Install `avisota/contao-demo` via composer
* Go to install tool and import `avisota-demo.sql`

Update guide
------------

```bash
mysqldump -u $dbuser -p \
    --skip-opt \
    --compact \
    --comments \
    --skip-create-options \
    --no-create-db \
    --no-create-info \
    --complete-insert \
    --hex-blob
    $database \
    tl_article \
    tl_content \
    tl_cron \
    tl_files \
    tl_form \
    tl_form_field \
    tl_layout \
    tl_log \
    tl_member \
    tl_member_group \
    tl_module \
    tl_nc_gateway \
    tl_nc_language \
    tl_nc_message \
    tl_nc_notification \
    tl_page \
    tl_search \
    tl_search_index \
    tl_session \
    tl_style \
    tl_style_sheet \
    tl_theme \
    tl_undo \
    tl_user \
    tl_user_group \
    orm_avisota_transport \
    orm_avisota_queue \
    orm_avisota_theme \
    orm_avisota_layout \
    orm_avisota_recipient_source \
    orm_avisota_mailing_list \
    orm_avisota_recipient_source_mailing_lists \
    orm_avisota_blacklist \
    orm_avisota_recipient \
    orm_avisota_subscription \
    orm_avisota_subscription_log \
    orm_avisota_message_category \
    orm_avisota_message \
    orm_avisota_message_content \
    orm_avisota_salutation_group \
    orm_avisota_salutation \
    > templates/avisota-demo.sql
```
